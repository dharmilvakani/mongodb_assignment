const Tasks = require('../models/tasks')

const createTask = (async (req, res) => {
    const task = new Tasks({
        ...req.body,
        owner: req.user._id
    })
    try {
        await task.save();
        res.status(201).send(task)
    } catch (error) {
        res.status(500).send(error);
    }
})

// GET /tasks?completed=true (for Searching)
// GET /tasks?limit=10&skip=20 (for limit and skip)
// GET /tasks?sortBy=createdAt:desc (for sorting)
const readTask = (async (req, res) => {
    const match = {}
    const sort = {}
    const search = req.query
    if (search.completed) {
        match.completed = search.completed === 'true'
    }
    if (req.query.sortBy) {
        const parts = req.query.sortBy.split(':')
        sort[parts[0]] = parts[1] === 'desc' ? -1 : 1
    }
    try {
        await req.user.populate({
            path: 'tasks',
            match,
            options: {
                limit: parseInt(req.query.limit),
                skip: parseInt(req.query.skip),
                sort
            }
        })
        res.send(req.user.tasks)

    } catch (error) {
        console.log(error)
        res.status(500).send(error)
    }
})

// const getTaskByAggregate = (async (req, res) => {
//     try {
//         console.log({ user: req.user })
//         const match = {}
//         const sort = { _id: -1 }
//         const skip = req.query.skip ? parseInt(req.query.skip) : 0
//         const limit = req.query.limit ? parseInt(req.query.limit) : 4

//         if (req.query.completed) {
//             match["completed"] = req.query.completed === "true"
//         }
//         if (req.query.sortBy) {
//             const parts = req.query.sortBy.split(":");
//             sort[parts[0]] = parts[1] === 'asc' ? 1 : -1
//         }
//         const task = await Tasks.aggregate([

//             { $match: { owner: req.user_id, ...match } },
//             { $sort: sort },
//             { $skip: skip },
//             { $limit: limit }
//         ])
//         res.status(200).json(task)
//     } catch (error) {
//         res.status(400).send(error.message)
//     }
// })

const readSingleTask = (async (req, res) => {
    const _id = req.params.id;
    try {
        const task = await Tasks.findOne({ _id, owner: req.user._id })
        if (!task) {
            res.status(404).send("Task not found!");
        }
        res.send(task);
    } catch (error) {
        console.log(error);
    }
})

const updateTask = (async (req, res) => {
    const updates = Object.keys(req.body)
    const allowedUpdate = ['desc', 'completed'];
    const validOperation = updates.every((update) => allowedUpdate.includes(update));
    if (!validOperation) {
        res.status(400).send({ error: "Invalid Updates" })
    }
    try {
        const task = await Tasks.findOne({ _id: req.params.id, owner: req.user._id })
        if (!task) {
            res.status(404).send();
        }
        updates.forEach((update) => task[update] = req.body[update])
        await task.save()
        res.send(task);
    } catch (error) {
        res.status(400).send()
    }
})

const deleteTask = (async (req, res) => {
    try {
        const task = await Tasks.findOneAndDelete({ _id: req.params.id, owner: req.user._id })
        if (!task) {
            res.status(404).send("Task not found");
        }
        res.send(task);
    } catch (error) {
        res.status(400).send()
    }
})
module.exports = { createTask, readTask, readSingleTask, updateTask, deleteTask }