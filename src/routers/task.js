const express = require('express');
const router = express.Router();
const { createTask, readTask, readSingleTask, updateTask, deleteTask } = require('../controllers/taskController')
const auth = require("../middleware/auth")

// Create Task
router.post("/", auth, createTask)

// Read all tasks
router.get("/", auth, readTask)

// Read Singular Task
router.get("/:id", auth, readSingleTask)

// Update Task by Id
router.patch("/:id", auth, updateTask)

// Delete Task by id
router.delete("/:id", auth, deleteTask)

// router.get("/aggregate", auth, getTaskByAggregate)


module.exports = router;